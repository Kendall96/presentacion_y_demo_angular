export class Student{
    id: number;
    name: string;
    email: string;
    phone: number;
}