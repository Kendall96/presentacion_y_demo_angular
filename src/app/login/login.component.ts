import { Component, OnInit } from '@angular/core';
import { DataService } from './../services/data.service';
import { User } from './../models/user';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import * as sha1 from 'js-sha512';
import { Ng2IzitoastService } from 'ng2-izitoast';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string;
  secret: string;
  password: string;
  token: string;
  users: User[] = [];
  valid: boolean;

  constructor(private dataService: DataService, private router: Router, public iziToast: Ng2IzitoastService) { }

  getUsers(){
    this.dataService.getUsers().subscribe(data => {
      this.users = data;
      console.log(this.users);
    });
  }

  verificar(){
    let user = this.users.find(x=> x.email == this.email && x.password == sha1.sha512(this.password));
    if(user == undefined){
      this.iziToast.error({title: "Error",
      message: 'The user does not exist!', position: 'topRight'});
    }else{
      this.dataService.verifyToken(user.secret, this.token).subscribe(data => {
        console.log(data);
        this.valid = data.valid;
        if(this.valid){
          sessionStorage.setItem('user',this.email);
          this.router.navigate(['/admin']);
        }else{
          this.iziToast.error({title: "Error",
          message: 'Incorrect Token!', position: 'topRight'});
        }
      });
    }
  }

  ngOnInit() {
    this.getUsers();
  }

}
